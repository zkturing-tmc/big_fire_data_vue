

**注意：该项目使用 element-ui@2.0.0+ 版本，所以最低兼容 vue@2.5.0**

## 前序准备

你的本地环境需要安装 [node](http://nodejs.org/) 和 [git](https://git-scm.com/)。我们的技术栈基于 [ES2015+](http://es6.ruanyifeng.com/)、[vue](https://cn.vuejs.org/index.html)、[vuex](https://vuex.vuejs.org/zh-cn/)、[vue-router](https://router.vuejs.org/zh-cn/) and [element-ui](https://github.com/ElemeFE/element)，提前了解和学习这些知识会对使用本项目有很大的帮助。


 **该项目不支持低版本浏览器(如ie)，有需求请自行添加`polyfill`


## 功能
```
- 登录/注销
- 权限验证
- 多环境发布
- 动态侧边栏（支持多级路由）
- 地图定位和标记
- websocket 实时通信
- 图片上传及坐标标记
- 文件上传
- Svg Sprite 图标
- Dashboard
- Echarts 图表
- 401/404错误页面
- 二步登录
```

## 开发
```bash
# 克隆项目

# 安装依赖
npm install
   
# 建议不要用cnpm安装 会有各种诡异的bug 可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```
浏览器访问 http://localhost:9527

## 发布
```bash
# 构建测试环境
npm run build:sit

# 构建生成环境
npm run build:prod
```
 
## 其它
```bash
# --report to build with bundle size analytics
npm run build:prod --report

# --preview to start a server in local to preview
npm run build:prod --preview

# lint code
npm run lint

# auto fix
npm run lint -- --fix
```

## License

[GPL](https://gitee.com/zkturing-tmc/big_fire_data_vue/blob/master/LICENSE)

Copyright (c) 2018-present turing.ac.cn


