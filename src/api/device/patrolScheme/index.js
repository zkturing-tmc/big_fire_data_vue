/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

//巡检计划列表
export function page(query) {
  return request({
    url: '/api/device/deviceInspectionScheme/pageList',
    method: 'get',
    params: query
  })
}

//巡检计划
export function addObj(obj) {
  return request({
    url: '/api/device/deviceInspectionScheme/add',
    method: 'post',
    data: obj
  })
}

//编辑添加巡检计划
export function addQuery(query) {
  return request({
    url: '/api/device/deviceInspectionScheme/get',
    method: 'get',
    params: query
  })
}

//编辑巡检计划
export function updateObj(obj) {
  return request({
    url: '/api/device/deviceInspectionScheme/update',
    method: 'post',
    data: obj
  })
}

//删除巡检计划
export function deleteObj(query) {
  return request({
    url: '/api/device/deviceInspectionScheme/delete',
    method: 'get',
    params: query
  })
}
