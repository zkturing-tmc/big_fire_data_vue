/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function page(query) {
    return request({
    url: '/api/device/deviceCollectingDevice/pageList',
    method: 'get',
    params: query
    })
}

export function selectQuery() {
    return request({
        url: '/api/device/deviceCollectingDevice/getSelected',
        method: 'get',
    })
}

export function createSelected(query) {
    return request({
        url: '/api/device/deviceCollectingDeviceType/selectType',
        method: 'get',
        params: query
    })
}

export function getFloorImg(query) {
    return request({
        url: '/api/device/deviceFloorLayout/selectImage',
        method: 'get',
        params: query
    })
}

export function addColObj(obj) {
    return request({
    url: '/api/device/deviceCollectingDevice/add',
    method: 'post',
    data: obj
    })
}

export function addObj(obj) {
    return request({
    url: '/api/device/deviceSensor/add',
    method: 'post',
    data: obj
    })
}

export function updateObj(obj) {
    return request({
    url: '/api/device/deviceSensor/update',
    method: 'post',
    data: obj
    })
}

export function getObj(id) {
return request({
url: '/api/device/deviceCollectingDevice/' + id,
method: 'get'
})
}

export function delObj(id) {
return request({
url: '/api/device/deviceCollectingDevice/delete?id=' + id,
method: 'get'
})
}

export function putObj(obj) {
    return request({
    url: '/api/device/deviceCollectingDevice/update',
    method: 'post',
    data: obj
    })
}

export function getAllInfoObj(id,obj) {
    return request({
    url: '/api/device/deviceSensorType/selectType?id=' + id,
    method: 'get',
    params: obj
    })
}
