/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'
// 右侧tree组件城市
export function fetchTree(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/select',
    method: 'get',
    params: query
  })
}

export function firstBuild(query) {
  //默认第一个区
  return request({
    url: '/api/device/deviceHardwareFacilities/selectFirst',
    method: 'get',
    params: query
  })
}

export function page(query) {
  //table列表数据
  return request({
    url: '/api/device/deviceHardwareFacilities/pageList',
    method: 'get',
    params: query
  })
}

export function getNetworkAll(query) {
  return request({
    url:'/api/device/deviceHardwareFacilities/get',
    method:'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/device/deviceHardwareFacilities/add',
    method: 'post',
    data: obj
  })
}

export function deletQuery(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/deleteQuery',
    method: 'get',
    params: query
  })
}

export function delObj(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/delete',
    method: 'get',
    params: query
  })
}

export function updateObj(obj) {
  return request({
    url: '/api/device/deviceHardwareFacilities/update',
    method: 'post',
    data: obj
  })
}

