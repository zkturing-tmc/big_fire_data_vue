/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

//室内巡检路线列表
export function pageInside(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/pageIndoorList',
    method: 'get',
    params: query
  })
}

//室内巡检添加前查询
export function insideAddQuery(obj) {
  return request({
    url: '/api/device/deviceInspectionRoute/addIndoor',
    method: 'post',
    data: obj
  })
}
//室内巡检设备table
export function pageFacility(query) {
  return request({
    url: '/api/device/deviceIndoorLabel/pageListRoute',
    method: 'get',
    params: query
  })
}
//室内巡检路线picklist
export function innerRoutes(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/getRouteIndoor',
    method: 'get',
    params: query
  })
}

//室内巡检路线pick
export function innerRouteAdd(obj) {
  return request({
    url: '/api/device/deviceInspectionRoute/addIndoorLabel',
    method: 'post',
    data: obj
  })
}
//室内巡检路线删除
export function deleteRoutes(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/deleteIndoorLabel',
    method: 'get',
    params: query
  })
}

//室内巡检路线更新
export function updateRoutes(obj) {
  return request({
    url: '/api/device/deviceInspectionRoute/updateName',
    method: 'post',
    data: obj
  })
}

//室内巡检路线列表删除
export function deleteListRoutes(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/deleteIndoor',
    method: 'get',
    params: query
  })
}

//室内巡检路线列表删除前查询
export function deleteListRoutesQuery(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/deleteQuery',
    method: 'get',
    params: query
  })
}

//室外巡检路线列表
export function pageOuter(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/pageOutdoorList',
    method: 'get',
    params: query
  })
}

//室外巡检路线列表删除
export function outerListDelete(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/deleteOutdoor',
    method: 'get',
    params: query
  })
}

//室外巡检路线添加
export function outerListAdd(obj) {
  return request({
    url: '/api/device/deviceInspectionRoute/addOutdoor',
    method: 'post',
    data: obj
  })
}

//室外巡检列表操作右侧列表
export function outerdoorList(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/getRouteOutdoor',
    method: 'get',
    params: query
  })
}

//室外巡检传感器地图坐标分布
export function outerMakerList(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/getAllRouteOutdoor',
    method: 'get',
    params: query
  })
}

//室外巡检传感器地图路线保存
export function outerHandleUpdate(obj) {
  return request({
    url: '/api/device/deviceInspectionRoute/updateOutdoor',
    method: 'post',
    data: obj
  })
}

//室外路线未标记设备
export function outerUnmarker(query) {
  return request({
    url: '/api/device/deviceInspectionRoute/getAllOutdoorSing',
    method: 'get',
    params: query
  })
}
