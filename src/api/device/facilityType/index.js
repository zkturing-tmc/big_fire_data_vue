/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function pageinside(query) {
  //table
  return request({
    url: '/api/device/deviceIndoorLabel/pageList',
    method: 'get',
    params: query
  })
}

export function facilityType(query) {
  //设施类型
  return request({
    url: '/api/device/deviceFacilitiesType/selectType',
    method: 'get',
    params: query
  })
}

export function facilityAddType(query) {
//  添加内的设施类型
  return request({
    url:'/api/device/deviceFacilitiesType/selectTypeByName',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  //添加室内设施
  return request({
    url: '/api/device/deviceIndoorLabel/add',
    method: 'post',
    data: obj
  })
}

export function facilityTypeEdit(query) {
//  编辑查询
  return request({
    url:'/api/device/deviceIndoorLabel/select',
    method: 'get',
    params: query
  })
}

export function facilityUpdate(obj) {
  //更新室内设施
  return request({
    url: '/api/device/deviceIndoorLabel/update',
    method: 'post',
    data: obj
  })
}

export function facilityDel(query) {
// 删除室内设施
  return request({
    url:'/api/device/deviceIndoorLabel/batchDelete',
    method: 'get',
    params: query
  })
}

//室外设施列表
export function pageOuter(query) {
  //table
  return request({
    url: '/api/device/deviceOutdoorLabel/pageList',
    method: 'get',
    params: query
  })
}

//室外设施高级查询条件-联网单位
export function netWorkingUnit(query) {
  //table
  return request({
    url: '/api/device/deviceNetworkingUnit/getAll',
    method: 'get',
    params: query
  })
}
//室外设施添加
export function addOuterObj(obj) {
  return request({
    url: '/api/device/deviceOutdoorLabel/add',
    method: 'post',
    data: obj
  })
}
//室外设施编辑查询
export function outerdoorEdit(query) {
  return request({
    url: '/api/device/deviceOutdoorLabel/select',
    method: 'get',
    params: query
  })
}
//室外设施编辑提交
export function outerdoorUpdate(obj) {
  return request({
    url: '/api/device/deviceOutdoorLabel/update',
    method: 'post',
    data: obj
  })
}
//室外设施编辑删除
export function outerdoorDel(query) {
  return request({
    url: '/api/device/deviceOutdoorLabel/batchDelete',
    method: 'get',
    params: query
  })
}
