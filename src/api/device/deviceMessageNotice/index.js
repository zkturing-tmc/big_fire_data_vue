/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

//通知方式
export function getNoticeType(query) {
  return request({
    url: '/api/device/deviceMessageNotice/selectByNoticeType',
    method: 'get',
    params: query
  })
}

//更改通知方式
export function changeNoticeType(obj) {
  return request({
    url: '/api/device/deviceMessageNotice/updateOrAdd',
    method: 'post',
    data: obj
  })
}
//接收人列表
export function page(query) {
  return request({
    url: '/api/device/deviceMessageRecipients/pagelist',
    method: 'get',
    params: query
  })
}
//删除接收人
export function delObj(query) {
  return request({
    url: '/api/device/deviceMessageRecipients/delete',
    method: 'get',
    params: query
  })
}

export function getNoticePeople(query) {
  return request({
    url: '/api/device/deviceMessageRecipients/findByMeassage',
    method: 'get',
    params:query
  })
}
//更新接通知人
export function updateObj(obj) {
  return request({
    url: '/api/device/deviceMessageRecipients/addBatch',
    method: 'post',
    data: obj
  })
}
