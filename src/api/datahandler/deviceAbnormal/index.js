/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function page(query) {
    return request({
    url: '/api/datahandler/deviceAbnormal/page',
    method: 'get',
    params: query
    })
}

export function addObj(obj) {
    return request({
    url: '/api/device/deviceAbnormal',
    method: 'post',
    data: obj
    })
}

export function getObj(id) {
    return request({
    url: '/api/datahandler/deviceAbnormal/' + id,
    method: 'get'
    })
}

export function delObj(id) {
    return request({
    url: '/api/datahandler/deviceAbnormal/' + id,
    method: 'delete'
    })
}

export function getAllStatus(query) {
    return request({
        url: '/api/device/deviceSensor/getAllStatusCount',
        method: 'get',
        params:query
    })
}

export function getBuildingList(query) {
    return request({
        url:'/api/device/deviceSensor/getStatusByBuild',
        methods:'get',
        params:query
    })
}

export function getUnhandleList(query) {
    return request({
        url: '/api/datahandler/deviceAbnormal/getAllAlrmAndFault',
        methods: 'get',
        params:query
    })
}

export function changeRsolve(query) {
    return request({
      url: "/api/datahandler/deviceAbnormal/affirmHandle",
      method: "post",
      data:query
    });
  }

  export function changeFireRsolve(query) {
    return request({
      url: "/api/datahandler/deviceAbnormal/affirmFire",
      method: "post",
      data:query
    });
  }

export function getAllListData(query) {
    return request({
    url: '/api/device/deviceSensor/getAllBuildingList',
    method: 'get',
    params: query
    })
}

export function getAllTenant(query) {
    return request({
        url: '/api/admin/tenant/pageList',
        method: 'get',
        params: query
    })
}

export function getTenantPos(query) {
    return request({
        url: '/api/device/deviceSensor/getStatusByBuildAndTenantId',
        method: 'get',
        params: query
    })
}
//消火栓地图

export function hydrantPoslist(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/selectByHydrantNameLike',
    method: 'get',
    params: query
  })
}
//消火栓故障数量
export function hydrantStatus(query) {
  return request({
    url: '/api/device/deviceSensor/getHydrantAllStatusCount',
    method: 'get',
    params: query
  })
}
//消火栓故障处理列表
export function hydrantRecordlist(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getAllAlrmAndFault',
    method: 'get',
    params: query
  })
}
//消火栓故障处理操作
export function hydrantRsolve(query) {
  return request({
    url: "/api/datahandler/deviceFacilitiesAbnormal/affirmHandle",
    method: "post",
    data:query
  });
}
//消火栓列表模式
export function hydrantAlllist() {
  return request({
    url: '/api/device/deviceHardwareFacilities/getAllHardwareFacilitiesList',
    method: 'get',
  })
}

export function getMaxAlarm(query) {
  return request({
    url: '/api/device/deviceBuilding/getMaxAlarmBuilding',
    method: 'get',
    params: query
  })
}
